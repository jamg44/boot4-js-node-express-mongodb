"use strict";

console.log('empiezo');

function escribeTrasDosSegundos(texto, callback) {
  setTimeout(function() {
    console.log('texto' + texto);
    callback();
  }, 2000);
}

function serie(n, func, callbackfin) {
  if (n <= 0) {
    if (callbackfin) callbackfin();
    return;
  }
  n = n - 1;
  func(n, function() {
    serie(n, func, callbackfin );
  })
}

serie(5, escribeTrasDosSegundos, function () {
  console.log('terminado');
});



