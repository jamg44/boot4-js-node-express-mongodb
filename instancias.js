"use strict";

// esta funcion la usamos como un constructor de objetos
function Fruta() {
  var nombre;
  var familia;

  this.setNombre = function(valor) { nombre = valor;};
  this.getNombre = function() { return nombre;};
  this.saluda = () => { console.log( 'hola, soy ', this.getNombre() )};

  this.color = 'amarillo';

  // para restringir las propiedades que tendrá el objeto resultante
  // return {
  //   setNombre: this.setNombre
  // }

}

/* forma literal de crear un objeto
var limon = {
  set...
};*/

// con un constructor

var limon = new Fruta();

console.log(limon);

limon.setNombre('limon');

console.log(limon.getNombre());

setTimeout(limon.saluda, 1000);