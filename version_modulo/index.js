"use strict";

// Leemos contenido de un fichero package.json

var fs = require('fs');
var async = require('async');

var versionModulo = function(nombreModulo, callback) {

  // calculamos la ruta al fichero
  var fichero = './node_modules/' + nombreModulo + '/package.json';

  fs.readFile(fichero, 'utf-8', function(err, data) {
    if (err) {
      console.log('Error!', err);
      return;
      // En asíncrono no usamos try/catch/throw new Error('Ops');
    }

    // convertimos el json a objeto
    var packageJson = JSON.parse(data);

    // llamamos al callback con la versión
    callback(null, packageJson.version);

  });

};

// función que devuelve la lista de los módulos en nuestro proyecto
var versionModulos = function(callback) {
  var ruta_modulos = './node_modules';

  fs.readdir(ruta_modulos, function(err, lista) {

    if (err) {
      callback(err);
      return;
    }

    async.concat(lista,
      function iterador(module, next) {

        // descartamos ficheros o carpetas que empiecen por .
        if (module[0] === '.') {
          next(null);
          return;
        }

        versionModulo(module, function(err, version) {
          if (err) {
            next(err);
            return;
          }
          next(null, { modulo: module, version: version});
          return;
        });

      },
      function finalizador(err, modulos) {
        callback(null, modulos);
        return;
      }
    );

  });

};


versionModulos(function(err, moduleArr) {
  if (err) {
    console.log('ERROR:', err);
    return;
  }
  console.log('Los módulos son:');
  moduleArr.forEach(function(module) {
    console.log('\t', module.modulo, module.version);
  });
});

