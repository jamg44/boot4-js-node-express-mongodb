"use strict";

var events = require('events');
var eventEmitter = new events.EventEmitter();

var suenaTelefono = function(quien) {
  // si llama mi madre que no suene el telefono, que llama mucho
  if (quien === 'madre') {
    return;
  }
  console.log('ring ring');
};

var vibrarTelefono = function(quien) {
  console.log('brrr brrr');
};

eventEmitter.on('llamada telefono', suenaTelefono);
eventEmitter.on('llamada telefono', vibrarTelefono);

eventEmitter.emit('llamada telefono', 'madre');
