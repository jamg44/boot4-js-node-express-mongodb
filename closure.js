"use strict";

function creaClosure(valor) {
  valor = valor + 1000;
  return function() { return valor; }
}

var clos1 = creaClosure(5);
var clos2 = creaClosure(20);

console.log(clos1());
console.log(clos2());
console.log(clos1());
