"use strict";

var basicAuth = require('basic-auth');

function auth(username, password) {
  return function(req, res, next) {
    var user = basicAuth(req);
    // si tenemos usuario lo validamos
    if (!user || user.name !== username || user.pass !== password) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      res.send(401);
      return;
    }
    next();
  };
}

module.exports = auth;
