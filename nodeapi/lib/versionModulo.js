"use strict";

var fs = require('fs');

var versionModulo = function(nombreModulo, callback) {

  // calculamos la ruta al fichero
  var fichero = './node_modules/' + nombreModulo + '/package.json';

  fs.readFile(fichero, 'utf-8', function(err, data) {
    if (err) {
      console.log('Error!', err);
      return;
      // En asíncrono no usamos try/catch/throw new Error('Ops');
    }

    // convertimos el json a objeto
    var packageJson = JSON.parse(data);

    // llamamos al callback con la versión
    callback(null, packageJson.version);

  });

};

module.exports = versionModulo;