"use strict";

var versionModulo = require('./versionModulo');
var fs = require('fs');
var async = require('async');

// función que devuelve la lista de los módulos en nuestro proyecto
var versionModulos = function(callback) {
  var ruta_modulos = './node_modules';

  fs.readdir(ruta_modulos, function(err, lista) {

    if (err) {
      callback(err);
      return;
    }

    async.concat(lista,
      function iterador(module, next) {

        // descartamos ficheros o carpetas que empiecen por .
        if (module[0] === '.') {
          next(null);
          return;
        }

        versionModulo(module, function(err, version) {
          if (err) {
            next(err);
            return;
          }
          next(null, { modulo: module, version: version});
          return;
        });

      },
      function finalizador(err, modulos) {
        callback(null, modulos);
        return;
      }
    );

  });

};

module.exports = versionModulos;