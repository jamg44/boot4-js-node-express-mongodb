var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  // segundo actual
  var segundo = (new Date()).getSeconds();

  // lista de usuarios
  var users = [
    {
      name: 'Smith'
    },
    {
      name: 'Thomas'
    }
  ];

  res.render('index', {
    title: 'Express',
    valor: '<script>alert("loquesea")</script>',
    condicion: {
      segundo: segundo,
      estado: segundo % 2 === 0
    },
    users: users
  });
});

module.exports = router;
