"use strict";

var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Agente = mongoose.model('Agente');

// lista de agentes
router.get('/', function(req, res, next) {

  var name = req.query.name;
  var age = req.query.age;

  var sort = req.query.sort || null;
  var limit = parseInt(req.query.limit) || null;
  var skip = parseInt(req.query.skip) || 0;
  var fields = req.query.fields || null;

  var filter = {};

  if (typeof name !== 'undefined') {
    filter.name = name;
  }

  if (typeof age !== 'undefined') {
    filter.age = age;
  }

  Agente.list(filter, sort, limit, skip, fields)
    .then(function(agentes) {
    res.json({success: true, agentes: agentes});
  }).catch(next);
});

// crear un agente
router.post('/', function(req, res, next) {

  var agente = new Agente(req.body);

  agente.save(function(err, agenteGuardado) {
    if (err) {
      next(err);
      return;
    }
    res.json({success: true, agente: agenteGuardado});
  });

});

// actualizar un agente
router.put('/:id', function(req, res, next) {
  var id = req.params.id;
  Agente.update({_id: id}, req.body, function(err, agente) {
    if (err) {
      next(err);
      return;
    }
    res.json({success: true, agente: agente});
  });
});

// eliminar un agente
router.delete('/:id', function(req, res, next) {
  var id = req.params.id;
  Agente.remove({_id: id}, function(err, result) {
    if (err) {
      return next(err);
    }
    res.json({succes: true, result: result});
  });
});


module.exports = router;
