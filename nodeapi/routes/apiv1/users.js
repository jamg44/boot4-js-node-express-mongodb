"use strict";

var express = require('express');
var router = express.Router();

var jwt = require('jsonwebtoken');

router.post('/login', function(req, res, next) {
  let user = req.body.user;
  let pass = req.body.pass;

  // busco el usuario en la BD y verifico la contraseña
  // en caso de no encontrar el usuario o no coincidir la contraseña
  // llamariamos a:
  // next(err);

  let userRecord = { id: 44, name: 'javi'};


  let token = jwt.sign({id: userRecord.id}, '123454567sdfsdfads', {
    expiresIn: '2 days'
  });

  res.json({success:true, token: token});
  // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDQsImlhdCI6MTQ3NzAwMjM0NCwiZXhwIjoxNDc3MTc1MTQ0fQ.lK_yPi4bBZ4yAOjTp6InP5K4tg-pyaAOEiDwpsMMr4g
});


router.get('/', function(req, res, next) {

  var list = [
    {
      name: 'Smith'
    },
    {
      name: 'John'
    }
  ];

  res.send({success: true, result: list});
});

router.get('/active/:id([0-9]+)/piso/:piso(A|B|C)', function(req, res, next) {
  console.log('parametros por ruta', req.params);
  res.send('ok, el usuario pedido es el ' + req.params.id);
});

router.get('/deleted', function(req, res) {
  console.log('recibido por query-string', req.query);
  console.log(req.get('Cache-Control')); // leer una cabecera
  res.send('ok, el usuario pedido es el ' + req.query.id);
});

router.post('/deleted', function(req, res) {
  console.log('recibido en el body', req.body);
  res.json({success:true});
});

router.get('/fichero', function(req, res) {
  res.download(__dirname + '/users.js', 'acelgas.js');
});

router.get('/redir', function(req, res) {
  res.redirect('http://google.es');
});

module.exports = router;