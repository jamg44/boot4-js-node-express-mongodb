"use strict";

function creaAgente(nombre) {
  var edad = 0;
  return {
    leeNombre: function() { return nombre; },
    leeEdad: function() { return edad; },
    ponEdad: function(value) { edad = value; },
    imprimeNombre: function() { console.log( nombre); }
  }
}

var smith = creaAgente('Smith');

console.log(smith.leeNombre());
console.log(smith.leeEdad());

smith.ponEdad(32);

console.log(smith.leeEdad());

setTimeout(smith.imprimeNombre, 1000);
