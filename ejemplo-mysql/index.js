"use strict";

var mysql = require('mysql');

var connection = mysql.createConnection({
  host: 'didimo.es',
  user: 'usuariocurso',
  password: 'us3r',
  database: 'cursonode'
});

connection.connect();

connection.query('SELECT * from agentes', function(err, rows, fields) {
  if (err) {
    console.log(err);
    return;
  }

  console.log(fields);

  for( var i = 0; i < rows.length; i++) {
    var agente = rows[i];
    console.log(agente.idagentes, agente.name, agente.age);
  }

  connection.end();

});